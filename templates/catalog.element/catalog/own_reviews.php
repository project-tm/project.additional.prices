<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if($arParams['USE_OWN_REVIEW'] != 'N'):?>
	<?
	$bAjax = (isset($_REQUEST['rz_ajax']) && $_REQUEST['rz_ajax'] === 'y');
	$arAvailableReviews = array();
	if (IsModuleInstalled('blog')) $arAvailableReviews[] = 'blog';
	if (IsModuleInstalled('forum')) $arAvailableReviews[] = 'forum';
	if (IsModuleInstalled('yenisite.feedback') && IsModuleInstalled('yenisite.bitronic2lite')) {
		$arAvailableReviews[] = 'feedback';
	}
	if (!in_array($arParams['REVIEWS_MODE'], $arAvailableReviews)) {
		$arParams['REVIEWS_MODE'] = $arAvailableReviews[0];
	}
	
	if(!empty($arResult["PROPERTIES"]["ID_TOVARA_NA_IANDEKS_MARKETE_DLIA_OTZYVOV"]["VALUE"]) && strlen($arResult["PROPERTIES"]["ID_TOVARA_NA_IANDEKS_MARKETE_DLIA_OTZYVOV"]["VALUE"]) > 0) {
		$APPLICATION->IncludeComponent(
			"yenisite:yandex.market_reviews_model", 
			"AVEL_ya_reviews", 
			array(
				"ACCESSTOKEN" => "zp4vQ1K39xW63pnu4A4dP2bdhL9orY",
				"CACHE_TIME" => "86400",
				"CACHE_TYPE" => "A",
				"COUNT" => "10",
				"GRADE" => "0",
				"HEAD" => "Отзывы о товаре:",
				"HEAD_SIZE" => "h2",
				"HOW" => "desc",
				"INCLUDE_JQUERY" => "Y",
				"MODEL" => $arResult["PROPERTIES"]["ID_TOVARA_NA_IANDEKS_MARKETE_DLIA_OTZYVOV"]["VALUE"],
				"SORT" => "date",
				"COMPONENT_TEMPLATE" => "AVEL_ya_reviews"
			),
			false
		);
	}
	
	//echo '<pre>'; print_r($arResult); echo '</pre>';
	//echo '<pre>'; print_r($arParams); echo '</pre>';
	
/*	if(!empty($arResult["PROPERTIES"]["ID_TOVARA_NA_IANDEKS_MARKETE_DLIA_OTZYVOV"]["VALUE"]) && strlen($arResult["PROPERTIES"]["ID_TOVARA_NA_IANDEKS_MARKETE_DLIA_OTZYVOV"]["VALUE"]) > 0) {
		$yandexReviewsModel = new yandexReviewsModel();
		$obCache = new CPHPCache;

//		if($obCache->InitCache(72000, serialize(array($getIdModelMarket, $ElementID)), "/iblock/catalog/reviewsyandex")) {
//			$arResultReviwe = $obCache->GetVars();
//		} else {

//			$resSearchYandex = $yandexReviewsModel->searchModelReviewsID($getIdModelMarket);
			$resSearchYandex = $yandexReviewsModel->searchModelReviewsID('13584660');
//		}

		foreach ($resSearchYandex["modelOpinions"]["opinion"] as $keyItem=>$arIrtem) {
			switch ($arIrtem["grade"]) {
				case '-2':
					$clasRating = '1';
					break;
				case '-1':
					$clasRating = '2';
					break;
				case '0':
					$clasRating = '3';
					break;
				case '1':
					$clasRating = '4';
					break;
				case '2':
					$clasRating = '5';
					break;
			}

			$arResultReviwe["ALL_BALL"] += $clasRating;
			$arResultReviwe["COUNT_REVIEWS"] += 1;
			$arResultReviwe["ITEMS_REVIEW"][] = $arIrtem;
		}

		$arResultReviwe["SRED_ARIFM"] = $arResultReviwe["ALL_BALL"] / $arResultReviwe["COUNT_REVIEWS"];
		
		d2($resSearchYandex, '$resSearchYandex', __FILE__,true, 1);  // Pulik Nikita 20161130
	}*/
	// d2($resSearchYandex, '$resSearchYandex', __FILE__,true, 1);  // Pulik Nikita 20161130
//	d2($arResult["PROPERTIES"]["ID_TOVARA_NA_IANDEKS_MARKETE_DLIA_OTZYVOV"], '$arResult["PROPERTIES"]["ID_TOVARA_NA_IANDEKS_MARKETE_DLIA_OTZYVOV"]', __FILE__,true, 1);
?>

	<?if(is_array($arResultReviwe["ITEMS_REVIEW"]) && count($arResultReviwe["ITEMS_REVIEW"]) > 0) {?>
		<section id="productreviews">

			<div class="feedback">
				<??>
				<div class="feedback_overall">
					<div class="feedback_info">
						<span class="rating rating_<?=ceil($arResultReviwe["SRED_ARIFM"])?> rating_big" title="<?=$arResultReviwe["SRED_ARIFM"];?>"></span>
						<span class="feedback_count">— средняя оценка на основе <?=$arResultReviwe["COUNT_REVIEWS"];?> <?=getNumEnding2($arResultReviwe["COUNT_REVIEWS"], array('отзыва', 'отзывов', 'отзывов'));?></span>
					</div>
					<div class="count-reviews">
						<a href="#productreviews"><?=$arResultReviwe["COUNT_REVIEWS"];?> <?=getNumEnding2($arResultReviwe["COUNT_REVIEWS"], array('отзыв', 'отзыва', 'отзывов'));?></a>
					</div>
					<div style="clear: both;"></div>
				</div>
				<?foreach($arResultReviwe["ITEMS_REVIEW"] as $key=>$arReviews) {?>

					<div class="feedback_item">
						<div class="feedback_info">
							<span class="feedback_author"><?=$arReviews["author"];?></span>

							<?switch ($arReviews["grade"]) {
								case '-2':
									$clasRating = '1';
									break;
								case '-1':
									$clasRating = '2';
									break;
								case '0':
									$clasRating = '3';
									break;
								case '1':
									$clasRating = '4';
									break;
								case '2':
									$clasRating = '5';
									break;
							}?>

							<span class="rating rating_<?=$clasRating;?> rating_medium" title="отлично!"></span>

							<time class="feedback_date" datetime="4">
								<?=ConvertTimeStamp(substr($arReviews["date"], 0, -3), 'SHORT');?>
							</time>

							<div class="feedback_content">
								<?if($arReviews["pro"]) {?>
									<p><b>Достоинства:</b><br/><?=$arReviews["pro"];?></p><br/>
								<?}?>
								<?if($arReviews["contra"]) {?>
									<p><b>Недостатки:</b><br/><?=$arReviews["contra"];?></p><br/>
								<?}?>
								<?if($arReviews["text"]) {?>
									<p><b>Комментарий:</b><br/><?=$arReviews["text"];?></p>
								<?}?>
							</div>
						</div>
					</div>
				<?}?>
			</div>
		</section>
	<?}?>



	<?if($arParams['REVIEWS_MODE'] == 'blog'):?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:blog.post.comment",
			"reviews",
			array(
				"SEO_USER" => "N",
				"ID" => $arResult['PROPERTIES'][CIBlockPropertyTools::CODE_BLOG_POST]['VALUE'],
				// for create new blog post
				"ELEMENT" => array(
					"DETAIL_PAGE_URL" => $arResult["DETAIL_PAGE_URL"],
					"NAME" => $arResult["NAME"],
					"PREVIEW_TEXT" => $arResult["PREVIEW_TEXT"],
				),

				"BLOG_URL" => $arParams['BLOG_URL'],
				"PATH_TO_SMILE" => $arParams["PATH_TO_SMILE"],
				"COMMENTS_COUNT" => $arParams["MESSAGES_PER_PAGE"],
				"DATE_TIME_FORMAT" => $DB->DateFormatToPhp(FORMAT_DATETIME),
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"AJAX_POST" => "N",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_HISTORY" => "N",
				"RZ_AJAX" => $bAjax,
				"SIMPLE_COMMENT" => "Y",
				"SHOW_SPAM" => "Y",
				"NOT_USE_COMMENT_TITLE" => "Y",
				"SHOW_RATING" => "N",
				"RATING_TYPE" => $arParams["RATING_TYPE"],
				"PATH_TO_POST" => $arResult["URL_TO_COMMENT"],
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"ELEMENT_ID" => $arResult['ID'],
				"NO_URL_IN_COMMENTS" => "L",
				"GAMIFICATION" => $arParams["SHOW_GAMIFICATION"],
				"RESIZER_COMMENT_AVATAR" => $arParams['RESIZER_SETS']['RESIZER_COMMENT_AVATAR']
			),
			$component,
			array("HIDE_ICONS" => "Y")
		);?>
	<?elseif($arParams['REVIEWS_MODE'] == 'feedback'):?>
		#YENISITE_FEEDBACK_CATALOG_COMMENTS#
	<?elseif($arParams['REVIEWS_MODE'] == 'forum'):?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:forum.topic.reviews",
			"reviews",
			Array(
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"MESSAGES_PER_PAGE" => $arParams["MESSAGES_PER_PAGE"],
				"USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
				"PATH_TO_SMILE" => $arParams["PATH_TO_SMILE"],
				"FORUM_ID" => $arParams["FORUM_ID"],
				"URL_TEMPLATES_READ" => $arParams["URL_TEMPLATES_READ"],
				"SHOW_LINK_TO_FORUM" => $arParams["SHOW_LINK_TO_FORUM"],
				"ELEMENT_ID" => $arResult['ID'],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"AJAX_POST" => $arParams["REVIEW_AJAX_POST"],
				"RZ_AJAX" => $bAjax,
				"POST_FIRST_MESSAGE" => $arParams["POST_FIRST_MESSAGE"],
				"URL_TEMPLATES_DETAIL" => "",
				"AUTOSAVE" => false,
				"PAGE_NAVIGATION_TEMPLATE" => ".default",
				"GAMIFICATION" => $arParams["SHOW_GAMIFICATION"],
				"PREORDER" => (!empty($arParams["PREORDER"])) ? $arParams["PREORDER"] : "N",
			),
			$component,
			array("HIDE_ICONS" => "Y")
		);?>
	<?endif?>
<?endif?>