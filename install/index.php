<?php

include (__DIR__ .'/../lib/user.php');

use Bitrix\Main\Application,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Project\Additional\Prices\User;

IncludeModuleLangFile(__FILE__);

class project_additional_prices extends CModule {

    public $MODULE_ID = 'project.additional.prices';

    function __construct() {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('PROJECT_ADDITIONAL_PRICES_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_ADDITIONAL_PRICES_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_ADDITIONAL_PRICES_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function DoInstall() {
        ModuleManager::registerModule($this->MODULE_ID);
        RegisterModuleDependences('main', 'OnAfterUserAdd', $this->MODULE_ID, "\Project\Additional\Prices\Event", "OnAfterUserAdd");
        RegisterModuleDependences('main', 'OnAfterUserUpdate', $this->MODULE_ID, "\Project\Additional\Prices\Event", "OnAfterUserUpdate");
        $rsUsers = CUser::GetList(($by = "personal_country"), ($order = "desc"), array(), array('ID'));
        while ($arUser = $rsUsers->Fetch()) {
            User::setGroup($arUser['ID']);
        }
        Loader::includeModule($this->MODULE_ID);
    }

    public function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    protected function GetConnection() {
        return Application::getInstance()->getConnection(LogsTable::getConnectionName());
    }

}
